# CK-Database #

Ever dreamed to build evolutive and modular relational databases ?

This is both a Framework and a Tool.
This is not an ORM. 

Software or database architects, SQL or .NET developers, CK-Database is made for us.

### Modular database design ###

### SQL Fragment injection to tackle modularity ###

### Database setup phases - no more runtime issues thanks to SQL parsing ###
