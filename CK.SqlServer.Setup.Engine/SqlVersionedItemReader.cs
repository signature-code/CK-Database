using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using CK.Core;
using CK.Setup;
using CSemVer;

namespace CK.SqlServer.Setup;

/// <summary>
/// Implements <see cref="IVersionedItemReader"/> on a Sql Server database. 
/// </summary>
public sealed class SqlVersionedItemReader : IVersionedItemReader
{
    /// <summary>
    /// Gets the current version of this store.
    /// </summary>
    public static int CurrentVersion => _upgradeScripts.Length;

    SHA1Value? _runSignature;
    bool _initialized;

    /// <summary>
    /// Initializes a new <see cref="SqlVersionedItemReader"/>.
    /// </summary>
    /// <param name="manager">The sql manager to use.</param>
    public SqlVersionedItemReader( ISqlManager manager )
    {
        Throw.CheckNotNullArgument( manager );
        Manager = manager;
    }

    internal readonly ISqlManager Manager;

    /// <summary>
    /// Initializes the tables and objects required to support item versioning.
    /// This is public since other participants may way want to setup this sub system.
    /// </summary>
    /// <param name="m">The sql manager to use.</param>
    public static void AutoInitialize( ISqlManager m )
    {
        var monitor = m.Monitor;
        using( monitor.OpenTrace( "Installing SqlVersionedItemRepository store." ) )
        {
            int ver = (int)(m.ExecuteScalar( _scriptCreateAndGetVersion ) ?? -1);

            if( ver == CurrentVersion )
            {
                monitor.CloseGroup( $"Already installed in version {CurrentVersion}." );
            }
            else
            {
                if( ver == -1 )
                {
                    monitor.CloseGroup( $"Installed first store version." );
                    ver = 0;
                }
                while( ver < CurrentVersion )
                {
                    monitor.Info( $"Upgrading from Version {ver} to {ver + 1}." );
                    m.ExecuteNonQuery( _upgradeScripts[ver++] );
                    m.ExecuteNonQuery( $"update CKCore.tItemVersionStore set ItemVersion = '{ver}' where FullName = N'CK.SqlVersionedItemRepository';" );
                }
            }
        }
    }

    /// <summary>
    /// Gets the "RunSignature" stored version.
    /// </summary>
    /// <param name="monitor">The monitor to use.</param>
    /// <returns>The stored SHA1. Can be the default Zero SHA1 (<see cref="SHA1Value.IsZero"/>).</returns>
    public SHA1Value GetSignature( IActivityMonitor monitor )
    {
        if( !_runSignature.HasValue )
        {
            var s = (string)Manager.ExecuteScalar( "if object_id('CKCore.tItemVersionStore') is not null select ItemVersion from CKCore.tItemVersionStore where FullName=N'RunSignature' else select '';" );
            SHA1Value.TryParse( s, out var v );
            _runSignature = v;
        }
        return _runSignature.Value;
    }

    /// <inheritdoc />
    /// <remarks>
    /// Reads the CKCore.tItemVersionStore table.
    /// </remarks>
    public OriginalReadInfo GetOriginalVersions( IActivityMonitor monitor )
    {
        var result = new List<VersionedTypedName>();
        var fResult = new List<VFeature>();
        if( !_initialized )
        {
            AutoInitialize( Manager );
            _initialized = true;
        }
        using( var c = new SqlCommand( "select FullName, ItemType, ItemVersion from CKCore.tItemVersionStore where FullName <> N'CK.SqlVersionedItemRepository'" ) { Connection = Manager.Connection } )
        using( var r = c.ExecuteReader() )
        {
            while( r.Read() )
            {
                string fullName = r.GetString( 0 );
                string itemType = r.GetString( 1 );
                if( itemType == "VFeature" )
                {
                    fResult.Add( new VFeature( fullName, SVersion.Parse( r.GetString( 2 ) ) ) );
                }
                else if( fullName != "RunSignature" )
                {
                    Version v;
                    if( !Version.TryParse( r.GetString( 2 ), out v ) )
                    {
                        throw new Exception( $"Unable to parse version for {fullName}: '{r.GetString( 2 )}'." );
                    }
                    result.Add( new VersionedTypedName( fullName, r.GetString( 1 ), v ) );
                }
            }
        }
        monitor.Trace( $"Existing VFeatures: {fResult.Select( f => f.ToString() ).Concatenate()}" );
        return new OriginalReadInfo( result, fResult );
    }

    /// <inheritdoc />
    public VersionedName? OnVersionNotFound( IVersionedItem item, Func<string, VersionedTypedName> originalVersions )
    {
        // Maps "Model.XXX" to "XXX" versions for default context and database.
        if( item.FullName.StartsWith( "[]db^Model.", StringComparison.Ordinal ) )
        {
            return originalVersions( "[]db^" + item.FullName.Substring( 11 ) );
        }
        // Old code: Handle non-prefixed FullName when not found.
        return item.FullName.StartsWith( "[]db^", StringComparison.Ordinal )
                ? originalVersions( item.FullName.Substring( 5 ) )
                : null;
    }

    /// <inheritdoc />
    public VersionedName? OnPreviousVersionNotFound( IVersionedItem item, VersionedName prevVersion, Func<string, VersionedTypedName> originalVersions )
    {
        // Maps "Model.XXX" to "XXX" versions for default context and database.
        if( prevVersion.FullName.StartsWith( "[]db^Model.", StringComparison.Ordinal ) )
        {
            return originalVersions( "[]db^" + prevVersion.FullName.Substring( 11 ) );
        }
        // Old code: Handle non-prefixed FullName when not found.
        return prevVersion.FullName.StartsWith( "[]db^", StringComparison.Ordinal )
                ? originalVersions( prevVersion.FullName.Substring( 5 ) )
                : null;
    }

    internal static string CreateTemporaryTableScript = @"declare @T table(F nvarchar(400) collate Latin1_General_BIN2 not null,T varchar(16) collate Latin1_General_BIN2 not null,V varchar(64) not null);";

    internal static string MergeTemporaryTableScript = @"
merge CKCore.tItemVersionStore as target
	using( select * from @T ) as source on target.FullName = source.F
	when matched then update set ItemType = source.T, ItemVersion = source.V
	when not matched by target then insert( FullName, ItemType, ItemVersion ) values( source.F, source.T, source.V );";

    static string _scriptCreateAndGetVersion = SqlCKCoreInstaller.EnsureCKCoreSchemaScript + @"
if OBJECT_ID('CKCore.tItemVersionStore') is null
begin
	create table CKCore.tItemVersionStore
	(
		FullName nvarchar(400) collate Latin1_General_BIN2 not null,
		ItemType varchar(16) collate Latin1_General_BIN2 not null,
		ItemVersion varchar(64) not null,
		constraint CKCore_PK_tItemVersionStore primary key(FullName)
	);
	if OBJECT_ID('CKCore.tItemVersion') is not null
	begin
		insert into CKCore.tItemVersionStore( FullName, ItemType, ItemVersion )
			select FullName, ItemType, ItemVersion from CKCore.tItemVersion where FullName <> N'CK.SqlVersionedItemRepository';
		drop table CKCore.tItemVersion;
	end
	insert into CKCore.tItemVersionStore( FullName, ItemType, ItemVersion ) values( N'CK.SqlVersionedItemRepository', '', '0' );
	select -1;
end
else
begin
	select convert( int, ItemVersion) from CKCore.tItemVersionStore where FullName = N'CK.SqlVersionedItemRepository';
end";

    const string _update1 = @"update CKCore.tItemVersionStore set FullName = stuff(FullName,6,8,'Model.') where FullName like '[[]]db^Objects.%'";
    const string _update2 = @"
create view CKCore.vVFeature
as
    select VFeature = FullName, Version = ItemVersion from CKCore.tItemVersionStore where ItemType='VFeature';";

    // The ItemVersion was 32 char we now need at least 40 to store the RunSignature SHA1.
    // Let's go for 64.
    const string _update3 = @"
  alter table CKCore.tItemVersionStore alter column ItemVersion varchar(64) not null;
";
    readonly static string[] _upgradeScripts = new[] { _update1, _update2, _update3 };

}
