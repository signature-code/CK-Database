#region Proprietary License
/*----------------------------------------------------------------------------
* This file (CK.Setupable.Runtime\StObj\IStObjSetupDynamicInitializer.cs) is part of CK-Database. 
* Copyright © 2007-2014, Invenietis <http://www.invenietis.com>. All rights reserved. 
*-----------------------------------------------------------------------------*/
#endregion


namespace CK.Setup;

/// <summary>
/// Dynamic initialization is the last step: the StObj have been initialized, ordered, and their corresponding <see cref="IMutableSetupItem"/> have been 
/// created and configured. This is where new <see cref="ISetupItem"/>s can be created and registered (typically as children of the item).
/// This interface can be supported by attributes on the structured object, by the object itself or injected in StObjSetupItemBuilder.
/// </summary>
public interface IStObjSetupDynamicInitializer
{
    /// <summary>
    /// When called, any dependent StObjs have already been initialized: initializers for a Generalization are called before the ones of its Specializations for instance.
    /// If an initializer requires its dependent object to be initialized (before some of its own initializations), it can use the <see cref="IStObjSetupDynamicInitializerState.PushAction">PushAction</see>
    /// of the <paramref name="state"/> to register actions that will be executed after its dependencies (but before any actions pushed by its dependencies).
    /// </summary>
    /// <param name="state">Context for dynamic initialization.</param>
    /// <param name="item">The setup item for the object slice.</param>
    /// <param name="stObj">The StObj (the object slice).</param>
    /// <remarks>
    /// When implemented by the structured object itself, it may be called multiple times: once for each StObj slice in 
    /// its hierarchy.
    /// </remarks>
    void DynamicItemInitialize( IStObjSetupDynamicInitializerState state, IMutableSetupItem item, IStObjResult stObj );

}
