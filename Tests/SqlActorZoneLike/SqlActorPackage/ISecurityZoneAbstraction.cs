using CK.Core;

namespace SqlActorPackage;

public interface ISecurityZoneAbstraction : IRealObject
{
    bool IAmHere();
}
