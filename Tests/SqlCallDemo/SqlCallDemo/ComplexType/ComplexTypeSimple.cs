using System;

namespace SqlCallDemo.ComplexType;


public class ComplexTypeSimple
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime CreationDate { get; set; }
    public int? NullableInt { get; set; }
}
