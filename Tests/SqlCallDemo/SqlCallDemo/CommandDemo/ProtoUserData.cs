namespace SqlCallDemo.CommandDemo;

public class ProtoUserData
{
    public string UserName { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
}
