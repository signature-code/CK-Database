namespace SqlCallDemo.PocoSupport;

public interface IThingWithPower : IThing
{
    int Power { get; set; }
}
