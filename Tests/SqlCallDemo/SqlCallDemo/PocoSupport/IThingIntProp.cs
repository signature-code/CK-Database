namespace SqlCallDemo.PocoSupport;

public interface IThingIntProp : IThing
{
    int IntProp { get; set; }
}
