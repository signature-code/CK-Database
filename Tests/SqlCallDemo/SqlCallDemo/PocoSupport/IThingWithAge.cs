namespace SqlCallDemo.PocoSupport;

public interface IThingWithAge : IThing
{
    int Age { get; set; }
}
